import random

if __name__ == '__main__':
    print('Das Zahlenraten Spiel!')
    print('----------------------')

    score = 0
    correctInput = False

    fromNumber = 0
    toNumber = 0

    while not correctInput:
        fromNumber = int(input('Von welcher Zahl soll gestartet werden: '))
        toNumber = int(input('Bis zur welcher Zahl soll generiert werden: '))

        if fromNumber > toNumber:
            print('Die erste Zahl sollte kleiner sein...')
        else:
            correctInput = True

    score = int((toNumber - fromNumber) / 3)
    winNumber = random.randint(fromNumber, toNumber)

    while True:
        print(winNumber)
        print('Deine aktuellen Punkte: ' + str(score))
        userInput = int(input('Gebe deinen Wert ein: '))
        if userInput < winNumber:
            print('Der Wert ist größer!')
            score = score - 1
        if userInput > winNumber:
            print('Der Wert ist kleiner!')
            score = score - 1
        if userInput == winNumber:
            break

        if score <= 0:
            print('Du hast verloren!!!!')
            exit()
    print('Herzlichen Glückwunsch! Du hast die richtige Zahl erraten!')
    print('Du hast ' + str(score) + ' Punkte erreicht!')