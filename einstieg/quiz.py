if __name__ == '__main__':
    print('Herzlich Willkommen zum Quiz!')
    print('-----------------------------')

    errors = 0

    questions = [{'Aus was besteht Orangensaft?': 1},
                 {'Wie viele Haare hat ein Mensch auf dem Kopf?': 2},
                 {'Ist das Sitzen auf dem Drucker während der Arbeitszeit erlaubt?': 1},
                 {'Was ist eine Kartoffel?': 0}]

    answers = [
        ['Aus Pilzen', 'Aus Orangen', 'Aus glücklichen Hühnern'],
        ['Keine', 'Viele', 'Jeder Mensch ist anders'],
        ['Ja', 'Nein', 'Jain'],
        ['Gemüse', 'Obst'],
    ]

    for q in range(len(questions)):
        print('Frage: ' + list(questions[q])[0])
        for a in range(len(answers[q])):
            print(str(a) + ' > ' + answers[q][a])
        answer = int(input('Gebe deine Antwort ein: '))
        rightAnswer = list(questions[q].values())[0]

        if answer != rightAnswer:
            errors = errors + 1
            print('Falsch! Diese richtige Antwort lautet: ' + answers[q][rightAnswer])
        else:
            print('Deine Antwort ist richtig!')

    print('Du hast ' + str(errors) + ' Fehler gemacht!')